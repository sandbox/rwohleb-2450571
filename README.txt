
This module is currently housed in the following sandbox:
https://www.drupal.org/sandbox/rwohleb/2450571

It addresses the issue from the following patch:
https://www.drupal.org/node/873936#comment-9709973
