<?php

/**
 * Expose node taxonomy terms as a context condition.
 */
class context_condition_node_taxonomy_override extends context_condition_node_taxonomy {
  /**
   * Settings form for variables.
   */
  function settings_form() {
    $form = array();

    $form['context_condition_node_taxonomy_autocomplete'] = array(
      '#title' => t('Use an autocomplete text field instead of a select box.'),
      '#type' => 'checkbox',
      '#default_value' => variable_get('context_condition_node_taxonomy_autocomplete', FALSE),
      '#description' => t('If your site has a lot of taxonomy terms and you notice the Context UI loading really slowly, or even crashing, then you should enable this option.')
    );

    return $form;
  }

  function condition_values() {
    $values = array();

    if (module_exists('taxonomy')) {
      // Only load the condition values if we are using a select field.
      if (!variable_get('context_condition_node_taxonomy_autocomplete', FALSE)) {
        foreach (taxonomy_get_vocabularies() as $vocab) {
          if (empty($vocab->tags)) {
            foreach (taxonomy_get_tree($vocab->vid) as $term) {
              $values[$term->tid] = check_plain($term->name);
            }
          }
        }
      }
    }

    return $values;
  }

  function condition_form($context) {
    $form = array();

    if (module_exists('taxonomy')) {
      $vocabularies = taxonomy_get_vocabularies();

      // Should we use an autocomplete field instead?
      if (variable_get('context_condition_node_taxonomy_autocomplete', FALSE)) {
        $tids = $this->fetch_from_context($context, 'values');
        $tags = array();

        // Try and load all the tags, grouped by vocabulary ID.
        if (is_array($tids)) {
          $terms = taxonomy_term_load_multiple($tids);
          foreach ($terms as $tid => $term) {
            $tags[$term->vid][$term->tid] = $term;
          }
        }

        $form['#tree'] = TRUE;

        $form['vocab'] = array(
          '#type' => 'fieldset',
          '#title' => '',
          '#description' => $this->description,
          '#collapsible' => FALSE,
        );

        // We need an autocomplete field for each for the vocabularies.
        foreach ($vocabularies as $vid => $vocabulary) {
          $form['vocab'][$vocabulary->vid] = array(
            '#type' => 'textfield',
            '#title' => t('%vocab vocabulary', array('%vocab' => $vocabulary->name)),
            '#autocomplete_path' => 'context-ui/taxonomy/autocomplete-fix/'. $vocabulary->vid,
            '#default_value' => !empty($tags[$vid]) ? taxonomy_implode_tags($tags[$vid]) : '',
          );
        }
      }
      // Else, use the regular select field.
      else {
        $form = parent::condition_form($context);
        $form['#type'] = 'select';
        $form['#size'] = 12;
        $form['#multiple'] = TRUE;
        $options = array();
        foreach ($vocabularies as $vid => $vocabulary) {
          $tree = taxonomy_get_tree($vid);
          if ($tree && (count($tree) > 0)) {
            $options[$vocabulary->name] = array();
            foreach ($tree as $term) {
              $options[$vocabulary->name][$term->tid] = str_repeat('-', $term->depth) . $term->name;
            }
          }
        }
        $form['#options'] = $options;
      }
    }

    return $form;
  }

  function condition_form_submit($values) {
    // Are we use an autocomplete field instead?
    if (variable_get('context_condition_node_taxonomy_autocomplete', FALSE)) {
      $tids = array();

      // We need to translate the resulting values into the same format the
      // select field uses. This way we maintain backward compatibility in case
      // the autocomplete field is disabled.
      $vocabularies = taxonomy_get_vocabularies();
      foreach ($vocabularies as $vid => $vocabulary) {
        if (!empty($values['vocab'][$vid])) {
          // Translate term names into actual terms.
          $typed_terms = drupal_explode_tags($values['vocab'][$vid]);
          foreach ($typed_terms as $typed_term) {
            // See if the term exists in the chosen vocabulary and return the tid.
            if ($possibilities = taxonomy_term_load_multiple(array(), array('name' => trim($typed_term), 'vid' => array($vid)))) {
              $term = array_pop($possibilities);

              if (!empty($term)) {
                $tids[$term->tid] = $term->tid;
              }
            }
          }
        }
      }

      return $tids;
    }
    // Else, use the regular select field submit handler.
    else {
      return parent::condition_form_submit($values);
    }
  }
}
